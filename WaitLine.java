import java.util.ArrayList;

public class WaitLine {
    private ArrayList<Integer> line ;
    private Integer lock ;

    public WaitLine() {
        line = new ArrayList<>() ;
        lock = new Integer(10) ;
    }

    public synchronized void push(int number) {
        synchronized (line) {
            ArrayList<Integer> newLine = new ArrayList<>() ;
            newLine.add(number) ;
            for(int x : line)
                newLine.add(x) ;
            line = new ArrayList(newLine) ;
        }
      //  System.out.println("notify");
        notify();
    }

    public synchronized int pop() {
            if (line.isEmpty()) {
                try {
                  //  System.out.println("wait");
                    wait();
                   // System.out.println("wait is done " + line.get(line.size() - 1));
                } catch (InterruptedException e) {
                    //e.printStackTrace();
                }
            }
            synchronized (line) {
                //System.out.println("thread " + id + " " + line.get(line.size() - 1));
                return line.remove((int) line.size() - 1);
            }
    }

    public int size() { return line.size() ; }
}
