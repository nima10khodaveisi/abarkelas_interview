from django.urls import path
from .views import create, get_list, update, delete, read

urlpatterns = [
    path('create/<slug:name>/<slug:number>',  create),
    path('get/', get_list),
    path('update/<slug:name>/<slug:number>', update),
    path('delete/<slug:name>/<slug:number>', delete),
    path('read/<slug:name>', read),
]