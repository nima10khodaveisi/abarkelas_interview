from django.shortcuts import render
from django.http import JsonResponse
from .models import Contact
from django.forms.models import model_to_dict
from django.core import serializers

# Create your views here.


def create(request, name, number):
    contact = Contact(name=name, number=number)
    contact.save()
    return JsonResponse({'status': 'ok'})


def get_list(request):
    print(Contact.objects.all().values())
    return JsonResponse(serializers.serialize('json', Contact.objects.all()), safe=False)


def update(request, name, number):
    contact = Contact.objects.filter(name=name)[0]
    contact.number = number
    contact.save()


def delete(request, name, number):
    Contact.objects.filter(name=name, number=number).delete()


def read(request, name):
    return JsonResponse(serializers.serialize('json', Contact.objects.filter(name=name)), safe=False)