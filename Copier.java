import java.io.*;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Scanner;

public class Copier {
    private String path1 = "/root/Desktop/path1" ;
    private String path2 = "/root/Desktop/path2" ;

    public void copy(File file) throws IOException {
        File[] myFiles = file.listFiles() ;
        for(File loopFile : myFiles) {
            if(loopFile.isDirectory()) {
                String path = loopFile.getAbsolutePath() ;
                String localPath = path.substring(path1.length());
                File otherFile = new File(path2 + localPath) ;
                otherFile.mkdir() ;
                copy(loopFile) ;
            } else {
                String path = loopFile.getAbsolutePath() ;
                String localPath = path.substring(path1.length());
                File otherFile = new File(path2 + localPath) ;

                if(!otherFile.exists()) {

                    System.out.println(otherFile.getName());
                    otherFile.createNewFile() ;
                }
                    FileInputStream instream = new FileInputStream(loopFile);
                    FileOutputStream outstream = new FileOutputStream(otherFile);

                    byte[] buffer = new byte[1024];

                    int length;
                    /*copying the contents from input stream to
                     * output stream using read and write methods
                     */
                    while ((length = instream.read(buffer)) > 0){
                        outstream.write(buffer, 0, length);
                    }

                    //Closing the input/output file streams
                    instream.close();
                    outstream.close();

                    System.out.println("File copied successfully!!");
            }
        }
    }

    public void start() {
        try {
            copy(new File(path1)) ;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
