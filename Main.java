import java.lang.reflect.Array;
import java.util.ArrayList;

class Pusher extends Thread {
    private ArrayList<Integer> pushed ;
    private WaitLine line ;

    public Pusher(WaitLine line) {
        this.line = line ;
        pushed = new ArrayList<>() ;
        for(int i = 0 ; i < 100 * 1000 ; ++i)
            pushed.add(i) ;
    }

    @Override
    public void run() {
        for(int x : pushed) {
            System.out.println("push " + x);
            line.push(x) ;
            try {
                Thread.sleep(5000) ;
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println("done");
    }
}

class Poper extends Thread {
    private WaitLine line ;
    private int id ;

    public Poper(WaitLine line, int id) {
        this.line = line ;
        this.id = id ;
    }

    @Override
    public void run() {
        for(int i = 0 ; i < 10 ; ++i) {
            System.out.println("thread " + id + " " + line.pop());
            try {
                Thread.sleep(5000) ;
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}

public class Main {
    public static void main(String[] args) throws InterruptedException {
     /*   WaitLine line = new WaitLine() ;
        Pusher pusher1 = new Pusher(line) ;
        Pusher pusher2 = new Pusher(line) ;
        Poper poper1 = new Poper(line, 1) ;
        Poper poper2 = new Poper(line, 2 ) ;
        poper1.start();
        poper2.start();
        pusher1.start();
       // pusher2.start();*/
        Copier copier = new Copier() ;
        copier.start() ;
    }
}
