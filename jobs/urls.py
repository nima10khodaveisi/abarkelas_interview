from django.urls import path
from .views import start_crawling_view, search, add_job

urlpatterns = [
    path('/start', start_crawling_view),
    path('search/', search),
    path('/create', add_job, name='add_job')
]