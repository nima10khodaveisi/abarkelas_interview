from django.db import models

# Create your models here.


class Job(models.Model):
    name_of_company = models.CharField(
        max_length=128,
        null=True, blank=True
    )
    name_of_job = models.CharField(
        max_length=128,
        null=True, blank=True
    )

    class LevelChoices(models.Choices):
        JUNIOR = 'Junior',
        SENIOR = 'Senior',
        INTERN = 'Internship'

    level = models.CharField(
        max_length=128,
        null=True, blank=True
      #  choices=LevelChoices
    )

    class TimeChoices(models.Choices):
        PART_TIME = 'Part time',
        FULL_TIME = 'Full time'

    time_of_job = models.CharField(
        max_length=128,
        null=True, blank=True
       # choices=TimeChoices
    )


class Tech(models.Model):
    name = models.CharField(
        max_length=128
    )
    job = models.ForeignKey(
        to=Job,
        on_delete=models.CASCADE
    )