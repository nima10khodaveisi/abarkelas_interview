from django.shortcuts import render
import requests
from bs4 import BeautifulSoup
from .models import Job, Tech
from django.db.models import Q
from django.http import JsonResponse
from django.core import serializers

# Create your views here.


def start_crawling_view(request):
    base_url = 'https://quera.ir/careers/jobs?page='
    for i in range(1, 20):
        url = base_url + str(i)
        req = requests.get(url)

        html = BeautifulSoup(req.content, features='html.parser')

        for item in html.find_all('div', class_='item featured'):
            # print(item)
            name_of_company = None
            name_of_job = None
            level = None
            time_of_job = None
            tech_list = ['']
            for meta in item.find_all('div', class_='meta'):
                name_of_company = meta.string
            for name in item.find_all('a', class_='ui w5 header'):
                name_of_job = name.find('span').string
                level = name.find('span', class_='job-level hide-in-mobile').string
            # print(item)
            time_of_job = item.find('span', class_='job-collab').string
            for tech in item.find_all('span', class_='ui small tech label'):
                tech_list.append(tech.get_text())
            for tech in item.find_all('span', class_='ui small tech label main'):
                tech_list.append(tech.get_text())

            job = Job(name_of_company=name_of_company, name_of_job=name_of_job, level=level, time_of_job=time_of_job)
            job.save()
            for tech in tech_list:
                tech_instance = Tech(name=tech, job=job)
                tech_instance.save()

        for item in html.find_all('div', class_='item'):
            # print(item)
            name_of_company = None
            name_of_job = None
            level = None
            time_of_job = None
            tech_list = ['']
            for meta in item.find_all('div', class_='meta'):
                name_of_company = meta.string
            for name in item.find_all('a', class_='ui w5 header'):
                name_of_job = name.find('span').string
                level = name.find('span', class_='job-level hide-in-mobile').string
            # print(item)
            if item.find('span', class_='job-collab') != None:
              time_of_job = item.find('span', class_='job-collab').string
            for tech in item.find_all('span', class_='ui small tech label'):
                tech_list.append(tech.get_text())
            for tech in item.find_all('span', class_='ui small tech label main'):
                tech_list.append(tech.get_text())

            job = Job(name_of_company=name_of_company, name_of_job=name_of_job, level=level, time_of_job=time_of_job)
            job.save()
            for tech in tech_list:
                tech_instance = Tech(name=tech, job=job)
                tech_instance.save()




















def search(request):
    if request.method == 'GET':
        company = request.GET['company']
       # job = request.GET['job']
        jobs_list = Job.objects.filter(Q(name_of_company__contains=company))
        return JsonResponse(serializers.serialize('json', jobs_list), safe=False)


def get_tags(request):
    if request.method == 'GET':
        tag = request.GET['tag']
        jobs = Job.objects.filter(pk=Tech.objects.filter(name=tag))


def add_job(request):
    if request.method == 'POST':
        name_of_company = request.POST['name_of_company']
        name_of_job = request.POST['name_of_job']
        level = request.POST['level']
        time_of_job = request.POST['time_of_job']
        job = Job(name_of_company=name_of_company, name_of_job=name_of_job, level=level, time_of_job=time_of_job)
        job.save()
        # techs = request.POST['techs']
        # for tech in techs:
        #     tech_instance = Tech(name=tech, job=job)
        #     tech_instance.save()
        return JsonResponse({'status': 'ok'})
    else:
        return render(request, 'add_job.html', {})