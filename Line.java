import java.util.ArrayList;

public class Line {
    private ArrayList<Integer> line ;

    public Line() {
        line = new ArrayList<>() ;
    }

    public void push(int number) {
        synchronized (line) {
            line.add(number) ;
        }
    }

    public void pop() {
        synchronized (line) {
            line.remove((int) line.size() - 1) ;
        }
    }

    public int size() { return line.size() ; }
}
