#include<bits/stdc++.h> 
using namespace std ; 


const int N = 1000 + 10 ; 

int n , dp[N][N] ; 
string s ; 


int32_t main() {
	cin >> s ; 
	n = (int)s.size() ; 
	for(int i = 0 ; i < n ; ++i)
		dp[i][i] = 1 ;
	for(int i = 0 ; i < n - 1 ; ++i) { 
		if(s[i] == s[i + 1])
			dp[i][i + 1] = 1; 
	}
	for(int len = 3 ; len <= n ; ++len) { 
		for(int i = 0 ; i + len - 1 < n ; ++i) {
			int j = i + len - 1 ; 
			int mx = 0 ; 
			if(s[i] == s[j]) { 
				mx = max(mx, dp[i + 1][j - 1] + 2) ; 
			}
			mx = max(mx, dp[i + 1][j]) ; 
			mx = max(mx, dp[i][j - 1]) ; 
			dp[i][j] = mx ;
			cout << "ehy " << i + 1 << ' ' << j + 1 << ' ' << dp[i][j] << endl ; 
		}
	}
	cout << dp[0][n - 1] << endl ; 
}

