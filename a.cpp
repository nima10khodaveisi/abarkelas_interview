#include<bits/stdc++.h> 
using namespace std ; 

string s ; 

int32_t main() { 
	cin >> s ; 
	int sum = 0 ; 
	bool okay = true ; 
	for(int i = 0 ; i < (int) s.size() ; ++i) { 
		char c = s[i] ; 
		if(c == '(') 
			sum++ ; 
		else
			sum-- ; 
		if(sum < 0)
			okay = false ; 
	}
	if(sum != 0) 
		okay = false ; 
	if(okay)
		cout << "Yes" ; 
	else 
		cout << "No" ; 
}
